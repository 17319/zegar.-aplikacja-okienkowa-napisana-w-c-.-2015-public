﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dd_7.Zegar_Timer
{
    public partial class Zegar : UserControl
    {
        Color kolorTla;
        public Color KolorTla
        {
            get { return kolorTla; }
            set { kolorTla = value; }
        }

        Color kolorLinii;
        public Color KolorLinii
        {
            get { return kolorLinii; }
            set { kolorLinii = value; }
        }

        Graphics graph;

        public Zegar()
        {
            InitializeComponent();
            kolorTla = System.Drawing.SystemColors.Control;
            KolorLinii = Color.Black;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            // analogowy
            int s = DateTime.Now.Second - 15;
            int m = DateTime.Now.Minute;
            int g = DateTime.Now.Hour;
            Point srodek = new Point(pictureBoxWizualizacja.Width / 2, pictureBoxWizualizacja.Height / 2);
            int srednica = Math.Min(pictureBoxWizualizacja.Width, pictureBoxWizualizacja.Height) - 1;
            Point p_s = dajPunkt(srodek, s, 60, srednica * 0.49);
            Point p_m = dajPunkt(srodek, m, 60, srednica * 0.4);
            Point p_g = dajPunkt(srodek, g, 12, srednica * 0.35);

            graph.Clear(kolorTla);
            graph.DrawLine(new Pen(kolorLinii), srodek, p_s);
            graph.DrawLine(new Pen(kolorLinii,2), srodek, p_m);
            graph.DrawLine(new Pen(kolorLinii,3), srodek, p_g);

            pictureBoxWizualizacja.Refresh();

            // cyfrowy dla sprawdzenia
            labelCzas.Text = DateTime.Now.ToString("HH:mm:ss");
        }

        private Point dajPunkt(Point srodek, double ilosc, int podzial, double dlugosc)
        {
            return new Point(
                Convert.ToInt32(srodek.X+Math.Cos(2*Math.PI*ilosc/podzial) * dlugosc),
                Convert.ToInt32(srodek.Y+Math.Sin(2*Math.PI*ilosc/podzial) * dlugosc));
        }

        private void Zegar_Load(object sender, EventArgs e)
        {
            pictureBoxWizualizacja.Image = new Bitmap(pictureBoxWizualizacja.Size.Width, pictureBoxWizualizacja.Size.Height);
            graph = Graphics.FromImage(pictureBoxWizualizacja.Image);
            graph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            labelCzas.BackColor = kolorTla;
            timer_Tick(null, null);
        }

        private void Zegar_Resize(object sender, EventArgs e)
        {
            Zegar_Load(sender, e);
        }
    }
}
