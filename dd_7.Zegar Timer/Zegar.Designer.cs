﻿namespace dd_7.Zegar_Timer
{
    partial class Zegar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.pictureBoxWizualizacja = new System.Windows.Forms.PictureBox();
            this.labelCzas = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWizualizacja)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // pictureBoxWizualizacja
            // 
            this.pictureBoxWizualizacja.Location = new System.Drawing.Point(11, 24);
            this.pictureBoxWizualizacja.Name = "pictureBoxWizualizacja";
            this.pictureBoxWizualizacja.Size = new System.Drawing.Size(356, 277);
            this.pictureBoxWizualizacja.TabIndex = 0;
            this.pictureBoxWizualizacja.TabStop = false;
            // 
            // labelCzas
            // 
            this.labelCzas.AutoSize = true;
            this.labelCzas.Location = new System.Drawing.Point(8, 8);
            this.labelCzas.Name = "labelCzas";
            this.labelCzas.Size = new System.Drawing.Size(52, 13);
            this.labelCzas.TabIndex = 1;
            this.labelCzas.Text = "labelCzas";
            // 
            // Zegar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelCzas);
            this.Controls.Add(this.pictureBoxWizualizacja);
            this.Name = "Zegar";
            this.Size = new System.Drawing.Size(370, 301);
            this.Load += new System.EventHandler(this.Zegar_Load);
            this.Resize += new System.EventHandler(this.Zegar_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxWizualizacja)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.PictureBox pictureBoxWizualizacja;
        private System.Windows.Forms.Label labelCzas;
    }
}
